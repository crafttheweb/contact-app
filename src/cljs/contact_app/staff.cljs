(ns ^:figwheel-always contact-app.staff
    (:require 
      [reagent.core :as reagent :refer [atom]]
      [ajax.core :refer [GET POST]]))

(enable-console-print!)

(defonce page-state (reagent/atom {:expanded-rows #{} 
                                  :sort-val nil
                                  :ascending true 
                                  :staff []}))

(def endpoint "https://api.contactapp.com/");

(defn ajax-error-handler [{:keys [status status-text]}]
  (println (str "something bad happened: " status " " status-text)))

(defn get-staff [ & {:keys [page page-size handler]
                     :or {page 1
                          page-size 500
                          handler nil}}]
  (GET (str endpoint "domain/3/staff")
    :response-format :json
    :keywords? true
    :params {"page" page "pageSize" page-size}
    :handler (fn [res]
               (let [staff (get-in res [:_embedded :staff])]
                 (swap! page-state assoc :staff staff)
                 (if handler (handler staff))))
    :headers {:Accept "application/hal+json"}
    :error-handler ajax-error-handler))

(defn get-headers [fields]
  "Return just the fields which are table headers"
  (filter #(:header %) fields))

(defn get-non-headers [fields]
  "Return the fields which aren't headers"
  (remove #(:header %) fields))

(defn get-field-value [field row]
  (if (contains? field :displayFn)
    ((:displayFn field) field row)
    ((:key field) row)))

(defn update-sort-value [new-val]
  (if (= new-val (:sort-val @page-state))
    (swap! page-state assoc :ascending (not (:ascending @page-state)))
    (swap! page-state assoc :ascending true))
  (swap! page-state assoc :sort-val new-val))

(defn sorted [staff]
  (if (:sort-val @page-state) 
  (let [sorted-contents (sort-by (:sort-val @page-state) staff)]
    (if (:ascending @page-state)
      sorted-contents
      (rseq sorted-contents)))
  staff))

(defn expanded? [staff-id]
  (some #{staff-id} (:expanded-rows @page-state)))

(defn row-expand! [staff-id]
  (swap! page-state update-in [:expanded-rows] conj staff-id))

(defn row-collapse! [staff-id]
  (swap! page-state update-in [:expanded-rows] disj staff-id))

(defn toggle-row-expand! [staff-id]
  (if (expanded? staff-id) (row-collapse! staff-id) (row-expand! staff-id)))

(defn row-arrow [field row]
  [:a {:href "#" :onClick (fn [e] 
                            (.preventDefault e) 
                            (toggle-row-expand! (:staff_id row)))}
  [:span {:class (str "glyphicon " (if (expanded? (:staff_id row)) "glyphicon-menu-up" "glyphicon-menu-down"))}]])

(defn row-email [field row]
  (let [email (:email row)]
  [:a {:href (str "mailto:" email)} email]))

(defn profile-image [field row]
  (let [src (get-in row [:_links :image :href])]
  [:img {:src src}]))

(defn header-row [header]
  (let [label (:label header)
        sortable (:sortable header)
        attributes (merge {:class (:cssClass header) 
                           :key (:key header)}
                     (when sortable 
                       {:on-click (fn [e]
                                    (.preventDefault e) 
                                    (update-sort-value (:key header)))}))]
    [:th attributes (if sortable [:a {:href "#"} label] label)]))

(defn standard-row [row fields]
  (let [staff-id (:staff_id row)
        id (str "staff-id-" staff-id)]
  [:tr {:key id 
        :id id
        ;:onClick #(toggle-row-expand! staff-id)
        :class (if (expanded? staff-id) "active")}
   (doall (for [field fields]
            [:td {:key (str (:staff_id row) (:key field))} (get-field-value field row)]))]))

(defn expanded-row [row fields]
  (let [staff-id (:staff_id row)
        id (str "staff-id-expanded" staff-id)
        dl-fields (remove #(= :profile (:key %)) fields)
        image-field (filter #(= :profile (:key %)) fields)]
    
    [:tr {:key id :id id :class "info"}
     [:td (profile-image image-field row)]
     [:td {:colSpan 3} 
      [:dl
       (for [field dl-fields] 
         (let [label (:label field)
               dt-key label
               dd-key (str staff-id label)]
         (list 
           [:dt {:key dt-key} (:label field)]
           [:dd {:key dd-key} (get-field-value field row)])))]]]))

(defn staff-table [fields]
  (let [headers (get-headers fields)
        non-headers (get-non-headers fields)
        rows (sorted (:staff @page-state))]
    [:div.table-reponsive
    [:table.table
     [:thead
      [:tr (for [header headers]
             (header-row header))]]
     [:tbody
      (doall (for [row rows]
               (list 
                 (standard-row row headers)
                 (when (expanded? (:staff_id row)) (expanded-row row non-headers)))))]]]))

(defn staff-page []
  (get-staff)
  (let [fields [
              {:key :first_name :label "First Name" :cssClass "first-name" :sortable true :header true}
              {:key :last_name :label "Last Name" :sortable true :header true}
              {:key :phone :label "Phone Number" :header true}
              {:key :expand :label "" :displayFn row-arrow :header true}
              {:key :profile :label "Profile Image" :displayFn profile-image}
              {:key :title :label "Title"}
              {:key :email :label "Email" :displayFn row-email}
              {:key :mobile :label "Mobile"}
              {:key :internal_extension :label "Internal Extension"}
              {:key :logon :label "Logon"}]]
  (fn [_]
  [:div [:div.page-header [:h1 "Staff"]]
   [staff-table fields]])))

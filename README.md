# Contact App

## This is a Clojure & ClojureScript project. 

To get it up and running install [Leiningen](http://leiningen.org/) then run 
```
#!bash

lein figwheel
```


This is using [Figwheel](https://github.com/bhauman/lein-figwheel) for hot code reload

This is using [Reagent](https://github.com/reagent-project/reagent) which is a ClojureScript React wrapper

**The code for the staff page is here src/cljs/contact_app/staff.cljs**